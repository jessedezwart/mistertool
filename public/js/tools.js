$(".module-run").click(function () {
    $(this).attr("disabled", true);
    var toolName = $(this).attr('id');
    var modal = $(this).closest('.modal');
    modal.modal('hide');

    modalInputFields = modal.find("input");
    var options = {};
    modalInputFields.each(
        function (index) {
            options[$(this).attr('id')] = $(this).val();
        }
    );
    var postBodyData = {
        "name": toolName,
        "options": options
    };
    var postBody = JSON.stringify(postBodyData);

    $.post("/api/tool/run", postBody, null, "json");
});

function startProcessTracking() {
    // todo: get process status via API
    $('#terminal-block').css("display", "block");
    setInterval(getRunningProcesses, 2000);
}

function getRunningProcesses() {
    $.ajax({
        url: "/api/tool/running",
        timeout: 2000,
        success: function (result) {
            $("#terminal-loading").hide();
            displayProcesses(result);
        }
    });
}

function displayProcesses(processIds) {
    for (var i = 0, len = processIds.length; i < len; i++) {
        // Check if terminal exists
        if (!$("#terminal_" + processIds[i]).length) {
            // If not create terminal
            createTerminal(processIds[i]);
            
            // Add show if its the first process
            $("#collapse_" + processIds[i]).addClass("show");
        }
        // Fill terminals with data
        getProcessOutput(processIds[i]);
    }
}

function createTerminal(id) {

    $element = `
        <div class="card">
            <div class="card-header" id="heading${id}">
            <h2 class="mb-0">
                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse_${id}" aria-expanded="true" aria-controls="collapse_${id}">
                Output #${id}
                </button>
            </h2>
            </div>

            <div id="collapse_${id}" class="collapse" aria-labelledby="heading_${id}" data-parent="#terminal-container">
            <div class="card-body">
                <pre><code id="terminal_${id}"></code></pre>
            </div>
            </div>
        </div>
    `;

    $('#terminal-container').append($element);
}

function getProcessOutput(id) {
    $.ajax({
        url: "/api/tool/" + id,
        timeout: 2000,
        success: function (result) {
            $("#terminal_" + id).html(result);
        }
    });
}

$( document ).ready(startProcessTracking());