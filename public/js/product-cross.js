$(document).ready(function () {
    $("#searchBySupplierForm").submit(function (e) {
        e.preventDefault();
        $("#loading-card").show();
        $.ajax({
            url: "/api/product/stock/supplierSku/" + btoa($("#supplierSkuInput").val()),
            timeout: 10000,
            error: function () {
                displayNoResult()
            }
        }).done(function (result) {
            $("#loading-card").hide();
            var product = result;
            createProductCard(product);
        });
    });

    $("#searchBySkuForm").submit(function (e) {
        e.preventDefault();
        $("#loading-card").show();
        $.ajax({
            url: "/api/product/stock/sku/" + btoa($("#skuInput").val()),
            timeout: 10000,
            error: function () {
                displayNoResult()
            }
        }).done(function (result) {
            $("#loading-card").hide();
            var product = result;
            createProductCard(product);
        });
    });

    function createProductCard(product) {

        $element = `
        <div class="card module" data-sku="${product.sku}">
            <div class="card-body">
                <a href="#" class="closeCardHref"><span data-feather="x" class="closeCardBtn"></span></a>
                <h5 class="card-title">${product.sku}</h5>
                <p class="card-text">
                    ${product.supplier_information.map(supplier => {
                        return `
                        <strong>${supplier.supplier_canonical}:</strong> ${supplier.supplier_sku}<br>
                        `.trim();
                    }).join('')}
                    <a href="https://www.misteragri.com/catalogsearch/result/?q=${product.sku}" target="_blank">MisterAgri <span data-feather="external-link"></span></a><br>
                    <a href="http://odoo.w4e.com/web#id=${product.product_template_id}&view_type=form&model=product.template" target="_blank">Odoo <span data-feather="external-link"></span></a>
                </p>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Location: ${product.location_name}</li>
                <li class="list-group-item">Available: ${product.quantity}</li>
            </ul>
            <!-- <div class="card-body">
                <div class="form-group">
                    <label>Move to location:</label>
                    <input type="text" class="form-control form-control-sm" placeholder="${product.location_name}">
                    <input type="submit" class="btn btn-secondary mb-2 btn-sm move-location-btn disabled" style="margin-top:6px" data-product_id="${product.product_id}">
                </div>
            </div> -->
        </div>
        `;

        $('#loading-card').after($element);
        feather.replace();

        if (!product.product_id) {
            var warningHtml = '<p>This product was not found in Odoo. Do you want to delete the sku from the cross database?</p>';
            warningHtml = warningHtml + '<button type="button" class="btn btn-outline-danger btn-sm delete-sku" data-sku="' + product.sku + '">Delete sku</button>'
            $(".card[data-sku='" + product.sku + "']").children(".card-body").eq(1).html(warningHtml);
        }

        $(".delete-sku").click(function () {
            var thisFirst = $(this);
            $.ajax({
                url: 'http://odoo.w4e.com:8080/sku/' + $(this).attr("data-sku"),
                type: 'DELETE',
                success: function () {
                    alert("Deleted from cross database");
                    thisFirst.closest(".card").hide();
                },
                error: function () {
                    alert("Could not delete from the database.");
                }
            });
        });

        $(".closeCardHref").click(function () {
            $(this).closest(".card").hide();
        });

        $(".move-location-btn").click(
            function () {
                var location = $(this).prev().val();
                if (!confirm("Are you sure you want to move all stock to " + location + "?")) {
                    return;
                }

                $.post({
                    url: "/api/product/stock/move/" + $(this).attr("data-product_id") + "/" + location,
                }).done(function () {
                    alert("Product was moved to " + location);
                }).fail(function (message) {
                    alert("Error: " + message);
                });
            }
        )

    }

    $('#loading-card').hide();

    function displayNoResult() {
        $('#loading-card').hide();
        $element = `
        <div class="card module">
            <div class="card-body">
                <a href="#" class="closeCardHref"><span data-feather="x" class="closeCardBtn"></span></a>
                <h5 class="card-title">No result</h5>
            </div>
        </div>
        `;
        $('#loading-card').after($element);
        feather.replace();
        $(".closeCardHref").click(function () {
            $(this).closest(".card").hide();
        });
    }



});