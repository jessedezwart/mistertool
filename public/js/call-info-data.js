$( document ).ready(function() {
    $(".order-block").dblclick(function() {
        window.open("http://odoo.w4e.com/web#id=" + $(this).attr("data-orderid") + "&view_type=form&model=sale.order");
    });

    $(".delivery-collapse").dblclick(function() {
        window.open("http://odoo.w4e.com/web#id=" + $(this).attr("data-deliveryid") + "&view_type=form&model=stock.picking");
    });

    $("#orderNumberInput").click(function() {
        $(this).attr("disabled", true);
        $("#search-form").first().submit();
    });
});