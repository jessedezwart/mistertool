<?php

namespace App\Tests\Provider;

use App\Provider\ToolProcessProvider;
use PHPUnit\Framework\TestCase;

class ToolProcessProviderTest extends TestCase{
    public function testFetchProcessIds() {
        $tpp = new ToolProcessProvider;
        $this->assertIsArray($tpp->fetchProcessIds());
    }

    public function testCreateId() {
        $tpp = new ToolProcessProvider;
        $this->assertIsInt($tpp->createId());
    }

    public function testDirConstructed() {
        $monitorDirPath = __DIR__ . "/../../data/process_logs";
        $tpp = new ToolProcessProvider;
        $this->assertDirectoryExists($monitorDirPath);
        $this->assertDirectoryIsWritable($monitorDirPath);
        $this->assertDirectoryIsReadable($monitorDirPath);
    }
}