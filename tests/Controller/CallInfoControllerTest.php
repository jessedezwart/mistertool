<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CallInfoControllerTest extends WebTestCase
{
    
    public function testRenderHome()
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRetrieveInfoViaPhoneNumber() 
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/search/phone/0653188926');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRetrieveInfoViaName() 
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/search/phone/Sjuk de Vries');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRetrieveInfoViaPostalCode() 
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/search/postal/1600');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRetrieveInfoViaOrderNumber() 
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/search/order/1-000009921');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testRetrieveInfoViaEmailAddress() 
    {
        $client = static::createClient();

        $client->request('GET', '/call-info/search/email/vanbeuningen.gijs@gmail.com');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testSearchPost() {
        $client = static::createClient();
        $client->request('POST', '/call-info/search', ['phoneNumber' => '0653188926']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->request('POST', '/call-info/search', ['postalCode' => '1600']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->request('POST', '/call-info/search', ['orderNumber' => '1-000009921']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->request('POST', '/call-info/search', ['emailAddress' => 'vanbeuningen.gijs@gmail.com']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $client->request('POST', '/call-info/search', ['name' => 'Sjuk de Vries']);
        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}