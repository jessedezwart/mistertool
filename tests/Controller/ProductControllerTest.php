<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductControllerTest extends WebTestCase
{
    
    public function testRenderHome()
    {
        $client = static::createClient();

        $client->request('GET', '/product/cross');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}