<?php

namespace App\Tests\Controller;

use App\Controller\ToolController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ToolControllerTest extends WebTestCase
{
    
    public function testRenderTools()
    {
        $client = static::createClient();

        $client->request('GET', '/tools/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}