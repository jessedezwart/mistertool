<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    
    public function testGetRunningProcesses()
    {
        $client = static::createClient();

        $client->request('GET', '/api/tool/running');
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetProductStockInfo()
    {
        $client = static::createClient();

        $client->request('GET', '/api/product/stock/sku/MTAzMDAwODA=');
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testGetProductStockInfoBySupplierSku()
    {
        $client = static::createClient();

        $client->request('GET', 'api/product/stock/supplierSku/NTI1OTM4MDQwLjMvMTA=');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
    }
}