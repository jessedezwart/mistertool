<?php

namespace App\Tests\Factory;

use App\Factory\ToolProcessFactory;
use PHPUnit\Framework\TestCase;

class ToolProcessFactoryTest extends TestCase{
    public function testFetchProcessIds() {
        $tpf = new ToolProcessFactory;
        $this->assertIsObject($tpf->create("test_object"));
    }
}