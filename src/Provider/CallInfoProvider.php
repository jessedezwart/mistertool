<?php

declare(strict_types=1);

namespace App\Provider;

use Carbon\Carbon;
use HttpClients\ClientFactory;
use HttpClients\OdooClientSync;
use Symfony\Component\Dotenv\Dotenv;

class CallInfoProvider
{
    /**
     * @var OdooClientSync
     */
    protected $odooCustomerClient;
    /**
     * @var OdooClientSync
     */
    protected $odooOrderClient;
    /**
     * @var OdooClientSync
     */
    protected $odooDeliveryClient;
    /**
     * @var OdooClientSync
     */
    protected $odooMoveLineClient;

    public const ODOO_FIELDS_CUSTOMER = [
        'name',
        'zip',
        'street',
        'city',
        'phone',
        'email',
        'total_invoiced',
        'parent_id'
    ];
    public const ODOO_FIELDS_ORDER = [
        'name',
        'date_order',
        'picking_ids',
        'amount_total',
        'date_order',
        'magento_order_status',
        'partner_id',
        'margin',
    ];
    public const ODOO_FIELDS_DELIVERY = [
        'name',
        'x_trackingcode',
        'state',
        'scheduled_date',
        'create_date',
        '__last_update',
        'weight',
        'move_lines',
    ];
    public const ODOO_FIELDS_MOVELINE = [
        'product_id',
        'product_tmpl_id',
        'price_unit',
        'product_qty',
        'availability',
        'product_uom_qty',
        'state',
    ];

    /**
     * @todo Put Odoo settings in .env
     */
    public function __construct()
    {
        $dotenv = new Dotenv(false);
        $dotenv->load(__DIR__.'/../../.env');

        $clientFactory = new ClientFactory();
        $this->odooCustomerClient = $clientFactory->getOdooSyncClient('res.partner', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooOrderClient = $clientFactory->getOdooSyncClient('sale.order', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooDeliveryClient = $clientFactory->getOdooSyncClient('stock.picking', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooMoveLineClient = $clientFactory->getOdooSyncClient('stock.move', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    /**
     * Retrieves all information by a phone number
     *
     * @param string $phoneNumber                  the phonenumber, can include netcodes as only the last eight digits are used
     * @param bool   $ignoreCustomersWithoutOrders set this if the provider shouldn't return customers without orders
     *
     * @return array the customers found
     */
    public function getAllInfoByPhoneNumber(string $phoneNumber, bool $ignoreCustomersWithoutOrders = true): array
    {
        $customers = $this->getCustomersByPhoneNumber($phoneNumber);

        return $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);
    }

    /**
     * Retrieves all information by zipcode
     *
     * @todo change zip code to postal code or the other way around for clarity
     *
     * @param string $zipcode                      the zipcode
     * @param bool   $ignoreCustomersWithoutOrders set this if the provider shouldn't return customers without orders
     *
     * @return array the customers found
     */
    public function getAllInfoByZipCode(string $zipcode, bool $ignoreCustomersWithoutOrders = true): array
    {
        $customers = $this->getCustomersByZipCode($zipcode);

        return $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);
    }

    public function getAllInfoByEmailAddress(string $emailAddress, bool $ignoreCustomersWithoutOrders = true): array
    {
        $customers = $this->getCustomersByEmailAddress($emailAddress);

        return $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);
    }

    public function getAllInfoByName(string $name, bool $ignoreCustomersWithoutOrders = true): array
    {
        $customers = $this->getCustomersByName($name);

        return $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);
    }

    private function getCustomersByEmailAddress(string $emailAddress): array
    {
        $criteria = [['email', 'ilike', $emailAddress], ['parent_id', '=', false]];

        $result = $this->odooCustomerClient->searchRead($criteria, 0, 20, self::ODOO_FIELDS_CUSTOMER);

        return $result->result->records;
    }

    private function getCustomersByName(string $name): array
    {
        $criteria = [['name', 'ilike', $name], ['parent_id', '=', false]];

        $result = $this->odooCustomerClient->searchRead($criteria, 0, 20, self::ODOO_FIELDS_CUSTOMER);

        return $result->result->records;
    }

    public function getAllInfoByOrderNumber(string $orderNumber, bool $ignoreCustomersWithoutOrders = false): ?array
    {
        $orderNumber = $this->buildRealOrdernumber($orderNumber);

        if (! $foundOrder = $this->getOrderByOrderNumber($orderNumber)) {
            return null;
        }

        // Get customer
        $customer = $this->getCustomerById($foundOrder->partner_id[0]);

        $customers = [];
        $customers[] = $customer;
        $customers = $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);

        // Put the order searched for at the top of the array so it gets highlighted on the frontend
        foreach ($customers[0]->mt_orders as $key => $order) {
            if ($order->name === $orderNumber) {
                $hydratedOrder = $customers[0]->mt_orders[$key];

                unset($customers[0]->mt_orders[$key]);
                array_unshift($customers[0]->mt_orders, $hydratedOrder);
            }
        }

        return $customers;
    }

    public function getAllInfoByDeliveryName(string $deliveryName, bool $ignoreCustomersWithoutOrders = false): ?array
    {

        if (! $foundDelivery = $this->getDeliveryByName($deliveryName)) {
            return null;
        }

        // Get customer
        $customer = $this->getCustomerById($foundDelivery->partner_id[0]);

        $customers = [];
        $customers[] = $customer;
        $customers = $this->hydrateCustomers($customers, $ignoreCustomersWithoutOrders);

        return $customers;
    }

    public function getDeliveryByName(string $deliveryName) {
        $criteria = [['name', 'ilike', $deliveryName]];

        $results = $this->odooDeliveryClient->searchRead($criteria, 0, 1, ['partner_id']);

        if (! empty($results->result->records)) {
            return $results->result->records[0];
        }

        return null;
    }

    private function buildRealOrdernumber(string $orderNumber): string
    {
        $orderNumber = str_pad($orderNumber, 9, '0', STR_PAD_LEFT);

        return '1-' . $orderNumber;
    }

    private function getOrderByOrderNumber(string $orderNumber): ?object
    {
        $criteria = [['name', '=', $orderNumber]];

        $results = $this->odooOrderClient->searchRead($criteria, 0, 1, ['partner_id']);
        if (! empty($results->result->records)) {
            return $results->result->records[0];
        }

        return null;
    }

    private function getCustomerById(int $id): object
    {
        $criteria = [['id', '=', $id], ['parent_id', '=', false]];

        $result = $this->odooCustomerClient->searchRead($criteria, 0, 1, self::ODOO_FIELDS_CUSTOMER);

        return $result->result->records[0];
    }

    /**
     * Get the array with customers anf filter out the customers without orders
     *
     * @return array customers
     */
    private function unsetCustomersWithoutOrders(array $customers): array
    {
        foreach ($customers as $key => $customer) {
            if (empty($customer->mt_orders)) {
                unset($customers[$key]);
            }
        }

        return $customers;
    }

    /**
     * Get all relevant order/delivery/moveline information for the customer via Odoo and fill the customer array
     *
     * @return array customers
     */
    private function hydrateCustomers(array $customers, bool $ignoreCustomersWithoutOrders = true): array
    {
        foreach ($customers as &$customer) {
            // Get all orders
            $customer->mt_orders = $this->getOrdersByCustomerId($customer->id);
            foreach ($customer->mt_orders as &$order) {
                $order->mt_deliveries = [];
                // Get all deliveries
                foreach ($order->picking_ids as $pickingId) {
                    $delivery = $this->getDeliveryById($pickingId)[0];
                    $delivery->mt_movelines = [];
                    // Get all movelines
                    foreach ($delivery->move_lines as $moveLineId) {
                        $moveline = $this->getMoveLineById($moveLineId)[0];
                        $delivery->mt_movelines[] = $moveline;
                    }
                    $order->mt_deliveries[] = $delivery;
                }
            }
        }

        // Filter customers if function argument says so
        if ($ignoreCustomersWithoutOrders) {
            $customers = $this->unsetCustomersWithoutOrders($customers);
        }

        // Sort orders and deliveries
        foreach ($customers as &$customer) {
            $customer->mt_orders = $this->sortItemsById($customer->mt_orders);
            foreach ($customer->mt_orders as &$order) {
                $order->mt_deliveries = $this->sortItemsById($order->mt_deliveries);
            }
        }

        return $customers;
    }

    private function sortItemsById(array $items): array
    {
        usort($items, function ($a, $b) {
            return $b->id - $a->id;
        });

        return $items;
    }

    /**
     * Get all customers by phonenumber
     *
     * @return array Customers
     */
    private function getCustomersByPhoneNumber(string $phoneNumber): array
    {
        // Strip phonenumber of net codes etc.
        $phoneNumber = $this->trimPhoneNumber($phoneNumber);

        $criteria = [['phone', 'ilike', $phoneNumber], ['parent_id', '=', false]];

        $result = $this->odooCustomerClient->searchRead($criteria, 0, 20, self::ODOO_FIELDS_CUSTOMER);

        return $result->result->records;
    }

    /**
     * Get all customers by zipcode
     *
     * @return array Customers
     */
    private function getCustomersByZipCode(string $zipcode): array
    {
        $criteria = [['zip', 'ilike', $zipcode], ['parent_id', '=', false]];

        $result = $this->odooCustomerClient->searchRead($criteria, 0, 20, self::ODOO_FIELDS_CUSTOMER);

        return $result->result->records;
    }

    /**
     * Get all orders by Odoo customer id
     *
     * @param int $customerId customer id
     *
     * @return array orders
     */
    private function getOrdersByCustomerId(int $customerId): array
    {
        // First get ids of orders
        $criteria = [['partner_id', 'child_of', $customerId]];
        $result = $this->odooOrderClient->searchRead($criteria, 0, 20, ['id']);
        foreach ($result->result->records as $record) {
            $this->odooOrderClient->callButton('get_magento_order_status', [$record->id]);
        }

        // Then get the rest of the information
        $criteria = [['partner_id', 'child_of', $customerId]];
        $result = $this->odooOrderClient->searchRead($criteria, 0, 20, self::ODOO_FIELDS_ORDER);
        foreach ($result->result->records as &$record) {
            $record->date_order_human = Carbon::parse($record->date_order)->diffForHumans();
        }

        return $result->result->records;
    }

    /**
     * Get delivery information by its Odoo id
     *
     * @return array delivery information
     */
    private function getDeliveryById(int $id): array
    {
        $criteria = [['id', '=', $id]];

        $result = $this->odooDeliveryClient->searchRead($criteria, 0, 1, self::ODOO_FIELDS_DELIVERY);

        $result->result->records[0]->min_date_human = Carbon::parse($result->result->records[0]->scheduled_date)->diffForHumans();
        $result->result->records[0]->__last_update_human = Carbon::parse($result->result->records[0]->__last_update)->diffForHumans();
        $result->result->records[0]->create_date_human = Carbon::parse($result->result->records[0]->create_date)->diffForHumans();

        return $result->result->records;
    }

    /**
     * Get moveline information by its Odoo id
     *
     * @return array moveline
     */
    private function getMoveLineById(int $id): array
    {
        $criteria = [['id', '=', $id]];

        $result = $this->odooMoveLineClient->searchRead($criteria, 0, 1, self::ODOO_FIELDS_MOVELINE);

        return $result->result->records;
    }

    /**
     * Gets the last 8 digits off the phonenumber.
     *
     * @return string phonenumber
     */
    private function trimPhoneNumber(string $phoneNumber): string
    {
        return mb_substr($phoneNumber, -8);
    }
}
