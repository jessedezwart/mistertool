<?php

declare(strict_types=1);

namespace App\Provider;

use App\Model\Service\Service;

class ServiceAdapter
{
    /**
     * @var Service
     */
    private $service;

    public function __construct(object $service)
    {
        $this->service = $service;
    }

    public function execute(): void
    {
        // execute the service
        $this->service->execute();
    }
}
