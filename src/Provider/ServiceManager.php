<?php

declare(strict_types=1);

namespace App\Provider;

use App\Model\Service\MultiToolService;
use App\ServiceOptionMapper;

class ServiceManager
{
    /**
     * @var array
     */
    private $options = [];

    public function fromMapper(string $commandName, array $inputOptions = []): void
    {
        $map = ServiceOptionMapper::getMap();
        $commandMap = $map[$commandName];

        // Replace placeholders in map with input options,
        //  placeholders are defined with brackets
        $commandMap['options'] = preg_replace_callback(
            '/(<<(.*)>>)/mU',
            function ($matches) use ($inputOptions) {
                return $inputOptions[$matches[2]];
            },
            $commandMap['options']
        );

        $this->options = $commandMap;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getService(): object
    {
        $serviceType = $this->getOptions()['type'];

        if ('MultiToolService' === $serviceType) {
            // @todo Redirect to builder
            // This code block is a placeholder till MultiTool is refactored
            $service = new MultiToolService();
            $service->setOptions($this->getOptions());
        } else {
            $serviceType = '\\App\\Model\\Service\\' . $serviceType;
            $service = new $serviceType();
            $service->setOptions($this->getOptions());
        }

        return $service;
    }
}
