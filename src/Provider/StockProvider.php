<?php

declare(strict_types=1);

namespace App\Provider;

use HttpClients\ClientFactory;
use HttpClients\OdooClientSync;
use stdClass;

class StockProvider
{
    /**
     * @var OdooClientSync
     */
    protected $odooProductClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockQuantClient;
    /**
     * @var OdooClientSync
     */
    protected $odooSupplierInfoClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockLocationClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockWarehouseOrderpointClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockPickingClient;

    public function __construct()
    {
        $clientFactory = new ClientFactory();
        $this->odooProductClient = $clientFactory->getOdooSyncClient('product.product', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockQuantClient = $clientFactory->getOdooSyncClient('stock.quant', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], 'admin', '0IsBN2j9mKfSrksBTPzx');
        $this->odooSupplierInfoClient = $clientFactory->getOdooSyncClient('product.supplierinfo', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockLocationClient = $clientFactory->getOdooSyncClient('stock.location', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockWarehouseOrderpointClient = $clientFactory->getOdooSyncClient('stock.warehouse.orderpoint', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockPickingClient = $clientFactory->getOdooSyncClient('stock.picking', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    public function getStockInformation(int $productProductId): ?object
    {
        $criteria = [
            ['product_id', '=', $productProductId],
            ['location_id.usage', '=', 'internal'],
        ];

        $result = $this->odooStockQuantClient->searchRead($criteria, 0, 100, ['quantity', 'location_id']);
        $records = $result->result->records;
        if (empty($records)) {
            return null;
        }

        $quantity = 0;
        foreach ($result->result->records as $record) {
            $quantity += $record->quantity;
        }
        $records[0]->quantity = $quantity;

        return $records[0];
    }

    public function createLocation(string $location): stdClass
    {
        $coordinates = $this->parseLocationCoordinates($location);
        $response = $this->odooStockLocationClient->createItem(
            [
                'location_id' => 12,
                'display_name' => 'MAI/Stock/' . $location,
                'company_id' => 1,
                'usage' => 'internal',
                'posx' => $coordinates['x'],
                'posy' => $coordinates['y'],
                'posz' => $coordinates['z'],
                'active' => true,
                'name' => $location,
                'return_location' => false,
            ]
        );
        $location = new stdClass();
        $location->id = $response->result;

        return $location;
    }

    private function parseLocationCoordinates(string $location): array
    {
        $location = explode('-', $location);

        return [
            'x' => str_pad($location[0], 3, '0', STR_PAD_LEFT),
            'y' => str_pad($location[1], 3, '0', STR_PAD_LEFT),
            'z' => str_pad($location[2], 3, '0', STR_PAD_LEFT),
        ];
    }

    public function getProductProductBySku(string $sku): ?stdClass
    {
        $criteria = [['default_code', '=', $sku]];

        $result = $this->odooProductClient->searchRead($criteria, 0, 1, ['id', 'seller_ids', 'product_tmpl_id']);
        $records = $result->result->records;

        if (empty($records[0])) {
            return null;
        }

        return $records[0];
    }

    public function getSellersByIds(array $sellerIds): array
    {
        $sellerIdInformation = [];
        foreach ($sellerIds as $id) {
            $criteria = [
                ['id', '=', (string) $id],
            ];
            $result = $this->odooSupplierInfoClient->searchRead($criteria, 0, 100, ['id', 'name']);

            $sellerIdInformation[] = $result->result->records[0];
        }

        return $sellerIdInformation;
    }

    public function parseStockLocationFromOdooString(string $stockLocation): string
    {
        $regex = '/\d{3}-\d{3}-\d{3}/m';

        preg_match_all($regex, $stockLocation, $matches, PREG_SET_ORDER, 0);

        if (empty($matches[0])) {
            return $stockLocation;
        }

        return $matches[0][0];
    }

    public function validateLocation(string $stockLocation): bool
    {
        $regex = '/\d{3}-\d{3}-\d{3}/m';

        preg_match_all($regex, $stockLocation, $matches, PREG_SET_ORDER, 0);

        if (empty($matches[0])) {
            return false;
        }

        return true;
    }
}
