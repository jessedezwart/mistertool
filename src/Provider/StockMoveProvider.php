<?php

declare(strict_types=1);

namespace App\Provider;

use HttpClients\ClientFactory;
use HttpClients\OdooClientSync;

class StockMoveProvider
{
    /**
     * @var OdooClientSync
     */
    protected $odooStockQuantClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockLocationClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockWarehouseOrderpointClient;
    /**
     * @var OdooClientSync
     */
    protected $odooStockPickingClient;

    public function __construct()
    {
        $clientFactory = new ClientFactory();
        $this->odooStockQuantClient = $clientFactory->getOdooSyncClient('stock.quant', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockLocationClient = $clientFactory->getOdooSyncClient('stock.location', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooStockWarehouseOrderpointClient = $clientFactory->getOdooSyncClient('stock.warehouse.orderpoint', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    public function moveStock(int $productProductId, string $locationName): void
    {
        $reorderRuleId = $this->getReorderRuleIdByProductProduct($productProductId);

        $destinationLocationId = $this->getLocationIdByName($locationName);

        $this->odooStockWarehouseOrderpointClient->updateItem($reorderRuleId, ['location_id' => $destinationLocationId]);

        // Accumulate all locations from the product
        $criteria = [
            ['product_id', '=', $productProductId],
            ['location_id.usage', '=', 'internal'],
        ];
        $fields = ['id'];
        $stockQuants = $this->odooStockQuantClient->searchRead($criteria, 0, 100, $fields)->result->records;

        foreach ($stockQuants as $stockQuant) {
            $this->odooStockQuantClient->updateItem($stockQuant->id, ['location_id' => $destinationLocationId]);
        }
    }

    private function getReorderRuleIdByProductProduct(int $productProductId): int
    {
        $odooStockWarehouseOrderpointClient = $this->odooStockWarehouseOrderpointClient;

        $criteria = [
            ['product_id', '=', $productProductId],
        ];

        $reorderRule = $odooStockWarehouseOrderpointClient->searchRead($criteria, 0, 1, ['id']);

        return $reorderRule->result->records[0]->id;
    }

    private function getLocationIdByName(string $locationName): int
    {
        $criteria = [
            ['name', '=', $locationName],
        ];
        $fields = ['location_id'];

        return $this->odooStockLocationClient->searchRead($criteria, 0, 1, $fields)->result->records[0]->id;
    }
}
