<?php

declare(strict_types=1);

namespace App\Provider;

use DirectoryIterator;

class ToolProcessProvider
{
    // Time after a process is considered expired
    public const PROCESS_EXPIRY_TIME = 120;

    /**
     * @var string Directory with outputs of processes
     */
    protected $monitorDir;

    public function __construct()
    {
        $this->monitorDir = $this->createMonitoringDir();
    }

    /**
     * Run the tool
     *
     * @todo Move this into ServiceManager/Adapter
     *
     * @param object $process Process
     */
    public function execute(object $process): void
    {
        $fileName = $this->monitorDir . '/' . $process->getId() . '.log';
        $logFp = fopen($fileName, 'w');
        chmod($fileName, 0777);
        ob_start(function ($buffer) use ($logFp): void {
            fwrite($logFp, $buffer);
        }, 1);

        $serviceManager = new ServiceManager();
        $serviceManager->fromMapper($process->getName(), $process->getOptions());
        $service = $serviceManager->getService();
        $serviceAdapter = new ServiceAdapter($service);
        $serviceAdapter->execute();
        ob_end_clean();
    }

    /**
     * Create the monitoring dir if not yet exists.
     * Also sets the working monitoring dir.
     *
     * @return string monitoring directory
     */
    private function createMonitoringDir(): string
    {
        $monitorDir = __DIR__ . '/../../data/process_logs';
        if (! file_exists($monitorDir)) {
            mkdir($monitorDir, 0777, true);
        }

        return $monitorDir;
    }

    /**
     * Fetches all running processes
     */
    public function fetchProcessIds(bool $running = false): array
    {
        $runningProcesses = [];
        $expiryTime = time() - $this::PROCESS_EXPIRY_TIME;
        // Check folder with std output for documents
        foreach (new DirectoryIterator($this->monitorDir) as $fileInfo) {
            $filename = $fileInfo->getFilename();
            $filepath = $this->monitorDir . '/' . $filename;
            // Skip file if not log
            if ($fileInfo->isDot() || 'log' !== pathinfo($filename, PATHINFO_EXTENSION)) {
                continue;
            }
            // Skip file is not running and only running are requested
            if (filemtime($filepath) < $expiryTime && $running) {
                continue;
            }

            $basename = pathinfo($filename, PATHINFO_FILENAME);
            $runningProcesses[] = (int) $basename;
        }

        return $runningProcesses;
    }

    /**
     * Create an Id that doesn't exist yet
     *
     * @return int id
     */
    public function createId(): int
    {
        return time();
    }

    /**
     * Get the output of a process
     *
     * @param int $id process id
     */
    public function getProcessOutput(int $id): ?string
    {
        $path = $this->monitorDir . '/' . $id . '.log';
        if (file_exists($path)) {
            return file_get_contents($path);
        }

        return null;
    }
}
