<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Controller\Api\ApiToolController;
use Symfony\Component\HttpKernel\Event\TerminateEvent;

class TerminateListener
{
    /**
     * This event will trigger after every response sent by this program.
     * If the requested uri matches /api/tool/run it will execute other code.
     *
     * Please see App\Controller\ApiController->postTool() for more information.
     */
    public function onKernelTerminate(TerminateEvent $event): void
    {
        $request = $event->getRequest();
        // Check if run tool request is made
        if ('/api/tool/run' === $request->getRequestUri()) {
            ini_set('max_execution_time', '0');
            $postBody = $request->getContent();
            $apiToolController = new ApiToolController();
            $apiToolController->execute($postBody);
        }

        return;
    }
}
