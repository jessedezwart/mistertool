<?php

declare(strict_types=1);

namespace App\Controller;

use App\ServiceOptionMapper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/tools")
 *
 * This controller provides the tools-page
 */
class ToolController extends AbstractController
{
    /**
     * Renders the homepage
     *
     * @Route("/")
     */
    public function renderTools(): object
    {
        // Fetch tools
        $tools = $this->fetchTools();

        return $this->render('tools.html.twig', [
            'modules' => $tools,
        ]);
    }

    /**
     * Fetches all tools from MisterTool
     *
     * @return array Tools
     */
    private function fetchTools(): array
    {
        // Get map from MisterTool
        $map = ServiceOptionMapper::getMap();

        // Format the array into a usable array for the Twig template
        $formattedMap = [];
        foreach ($map as $key => $tool) {
            $formattedTool = [
                'slug' => $this->cleanString($key),
                'name' => $key,
                'desc' => '',   //Descriptions not yet added to ServiceOptionMapper
                'options' => [],
            ];

            $regex = '/<<(\S*)>>/m';

            foreach ($tool['options'] as $option) {
                preg_match_all($regex, $option, $matches, PREG_SET_ORDER, 0);
                foreach ($matches as $match) {
                    $formattedTool['options'][] = $match[1];
                }
            }

            $formattedMap[] = $formattedTool;
        }

        return $formattedMap;
    }

    /**
     * Cleans strings
     *
     * @param string $string to clean
     *
     * @return string Cleaned string
     */
    private function cleanString(string $string): string
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }
}
