<?php

declare(strict_types=1);

namespace App\Controller;

use HttpClients\ClientFactory;
use MultiTool\Communicator\OdooConnector;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Controller for managing rest calls
 *
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * Create Odoo call
     *
     * @Route("/odoo"), methods={"POST"}
     */
    public function postOdoo(Request $request): Response
    {
        $postBody = json_decode($request->getContent(), true);

        $model = $postBody['model'];
        $crits = $postBody['criteria'];
        $fields = $postBody['fields'];

        $odooCon = new OdooConnector($model, $crits, $fields);

        $data = $odooCon->current();

        $response = new Response();
        $response->setContent(json_encode($data));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Create Odoo call
     *
     * @Route("/odoo/write"), methods={"POST"}
     */
    public function postOdooWrite(Request $request): Response
    {
        $postBody = json_decode($request->getContent(), true);
        $model = $postBody['model'];
        $id = $postBody['id'];
        $data = $postBody['data'];

        $odooCon = new OdooConnector($model, [[]], []);
        $res = $odooCon->write($id, $data);

        $response = new Response();
        $response->setContent(json_encode($res));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Create Odoo call
     *
     * @Route("/odoo/button"), methods={"POST"}
     */
    public function postOdooButton(Request $request): Response
    {
        $postBody = json_decode($request->getContent(), true);

        $id = $postBody['id'];
        $model = $postBody['model'];
        $method = $postBody['method'];
        $saleId = $postBody['active_id'];

        $cf = new ClientFactory();
        $odoo = $cf->getOdooSyncClient(
            $model,
            'http://odoo.w4e.com',
            [],
            'misteragri',
            'koppeling@misteragri.com',
            'W4e@koppeling'
        );

        $res = $odoo->callButton($method, [[$id],
            [
                'active_id' => $saleId,
                'active_ids' => [$saleId],
                'active_model' => 'sale.order',
            ],
        ]);

        $response = new Response();
        $response->setContent(json_encode($res));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
