<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Factory\ToolProcessFactory;
use App\Provider\ToolProcessProvider;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Controller for managing rest calls related to running and retrieving information about tools
 *
 * @Route("/api/tool")
 */
class ApiToolController extends AbstractController
{
    /**
     * This function gets called when a MisterTool tool needs to be run.
     * It starts a session and returns a 200 code.
     * After returning a response Symfony will throw the kernel.terminate event.
     * In ../EventListener/TerminateListener.php you'll find a function that get's triggered
     *  when this event happens.
     * This allows us to continue executing code after returning a response to the user.
     * For this method to work, this application needs to be run on PHP-FPM.
     *
     * @Route("/run"), methods={"POST"}
     */
    public function postTool(): Response
    {
        // Just return ok, message received and the tool will start executing the tool
        return new Response('Ok', Response::HTTP_OK);
    }

    /**
     * This is the function that get's called by the function in the TerminateListener.
     * It will check if a process is already running, if it's not, and for how long.
     * If nothing's running it will run the process.
     */
    public function execute(string $postBody): void
    {
        // Decode body
        $postBodyDecoded = json_decode($postBody, true);
        $toolName = $postBodyDecoded['name'];
        $toolOptions = $postBodyDecoded['options'];

        try {
            // Generate process id for later retrieving running processes
            $process = ToolProcessFactory::create($toolName);
            $process->setOptions($toolOptions);
            $processProvider = new ToolProcessProvider();
            $processProvider->execute($process);
        } catch (\Throwable $th) {
            // Send slack message to misterbot
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://hooks.slack.com/services/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',
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{"text":"MisterTool fail: ' . $th->getMessage() . '"}',
                CURLOPT_HTTPHEADER => [
                    'Content-Type: application/json',
                ],
            ]);
            curl_exec($curl);
            curl_close($curl);

            throw new Exception($th->getMessage());
        }

        return;
    }

    /**
     * Get the running processes
     *
     * @Route("/running"), methods={"get"}
     */
    public function getRunningProcesses(): object
    {
        $processProvider = new ToolProcessProvider();
        $runningProcesses = $processProvider->fetchProcessIds(true);

        $runningProcesses = json_encode($runningProcesses);

        $response = new Response($runningProcesses, Response::HTTP_OK);
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Get the log of a tool by id
     *
     * @Route("/{id}"), methods={"get"}
     */
    public function getTool(int $id): Response
    {
        $processProvider = new ToolProcessProvider();
        if ($processOutput = $processProvider->getProcessOutput($id)) {
            return new Response($processOutput, Response::HTTP_OK);
        }

        return new Response('No process with that ID found', Response::HTTP_NOT_FOUND);
    }
}
