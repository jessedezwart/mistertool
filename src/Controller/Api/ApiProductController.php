<?php

declare(strict_types=1);

namespace App\Controller\Api;

use App\Model\StockProduct;
use App\Provider\StockMoveProvider;
use App\Provider\StockProvider;
use CrossClient\Provider\SkuProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Controller for managing rest calls related to products
 *
 * @Route("/api/product")
 */
class ApiProductController extends AbstractController
{
    /**
     * Get info about stock status
     *
     * @todo Clean up this function
     * @Route("/stock/sku/{sku}", name="getProductStockInfo"), methods={"get"}
     */
    public function getProductStockInfo(string $sku): Response
    {
        $sku = base64_decode($sku, true);
        $stockProduct = new StockProduct($sku);
        $skuProvider = new SkuProvider();
        $supplierSkus = $skuProvider->getSupplierSkus($sku);
        
        if ($supplierSkus) {
            $stockProduct->setSupplierInformation($supplierSkus);
        }
        
        $stockProvider = new StockProvider();
        $odooProductProduct = $stockProvider->getProductProductBySku($sku);

        if (! empty($odooProductProduct)) {
            $stockProduct->setOdooProductProductId($odooProductProduct->id);
            $stockProduct->setOdooProductTemplateId($odooProductProduct->product_tmpl_id[0]);
            $stockInfo = $stockProvider->getStockInformation($odooProductProduct->id);

            if ($stockInfo) {
                $stockProduct->setLocationId($stockInfo->location_id[0]);
                $stockProduct->setLocationName($stockProvider->parseStockLocationFromOdooString($stockInfo->location_id[1]));
                $stockProduct->setQuantity($stockInfo->quantity);
            }
        }

        return new JsonResponse($stockProduct->jsonSerialize());
    }

    /**
     * Get stock info by supplier sku
     *
     * @Route("/stock/supplierSku/{supplierSku}"), methods={"get"}
     */
    public function getProductStockInfoBySupplierSku(string $supplierSku): Response
    {
        $supplierSku = base64_decode($supplierSku, true);
        $skuProvider = new SkuProvider();
        $sku = $skuProvider->getSku($supplierSku);
        if (! $sku) {
            return new Response("Couldn't find a sku for that supplier sku.", Response::HTTP_NOT_FOUND);
        }
        $sku = base64_encode($sku);

        return $this->redirectToRoute('getProductStockInfo', ['sku' => $sku]);
    }

    /**
     * Move product to a new location in the warehouse
     *
     * @Route("/stock/move/{productId}/{location}"), methods={"post"}
     */
    public function postMoveStock(int $productId, string $location): Response
    {
        $stockProvider = new StockMoveProvider();

        try {
            $stockProvider->moveStock($productId, $location);
        } catch (\Throwable $th) {
            return new Response($th->getMessage(), 500);
        }

        return new Response();
    }
}
