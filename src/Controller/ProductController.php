<?php

declare(strict_types=1);

namespace App\Controller;

use CrossClient\Provider\SkuProvider;
use CrossClient\Provider\SupplierProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api Controller for managing all product related page requests
 *
 * @Route("/product")
 */
class ProductController extends AbstractController
{
    /**
     * @route("/cross")
     */
    public function renderCrossHome(): Response
    {
        return $this->render('product-cross.html.twig');
    }

    /**
     * @route("/control", methods={"GET"}, name="renderControl")
     */
    public function renderProductControl(Request $request): Response
    {
        $newSku = $request->get("newSku");
        $removedSku = $request->get("removedSku");
        $supplierProvider = new SupplierProvider();

        $suppliers = $supplierProvider->getSuppliers();

        return $this->render('product-control.html.twig', ['suppliers' => $suppliers, "newSku" => $newSku, "removedSku" => $removedSku]);
    }

    /**
     * @route("/control/create", methods={"POST"})
     */
    public function createSku(Request $request) {
        $skuProvider = new SkuProvider();
        $response = $skuProvider->createSku(
            $request->get("range"),
            $request->get("supplier"),
            $request->get("supplierSku")
        );

        return $this->redirectToRoute("renderControl", ["newSku" => $response]);
    }

    /**
     * @route("/control/delete", methods={"POST"})
     */
    public function deleteSku(Request $request) {
        $sku = $request->get("sku");

        $skuProvider = new SkuProvider();
        $skuProvider->deleteSku($sku);

        return $this->redirectToRoute("renderControl", ["removedSku" => $sku]);
    }
}
