<?php

declare(strict_types=1);

namespace App\Controller;

use App\Provider\CallInfoProvider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/call-info")
 */
class CallInfoController extends AbstractController
{
    /**
     * @route("/", name="callInfoHome")
     */
    public function renderHome(): Response
    {
        return $this->render('call-info-home.html.twig');
    }

    /**
     * @route("/search"), methods={"POST"}
     */
    public function searchPost(Request $request): Response
    {
        $phoneInput = $request->request->get('phoneInput');
        $postalCodeInput = $request->request->get('postalCodeInput');
        $orderNumberInput = $request->request->get('orderNumberInput');
        $emailAddressInput = $request->request->get('emailAddressInput');
        $nameInput = $request->request->get('nameInput');
        $deliveryInput = $request->request->get('deliveryInput');
        if ($phoneInput) {
            return $this->redirectToRoute('searchByPhone', ['phoneNumber' => $phoneInput]);
        } elseif ($postalCodeInput) {
            return $this->redirectToRoute('searchByPostal', ['postalCode' => $postalCodeInput]);
        } elseif ($orderNumberInput) {
            return $this->redirectToRoute('searchByOrderNumber', ['orderNumber' => $orderNumberInput]);
        } elseif ($emailAddressInput) {
            return $this->redirectToRoute('searchByEmailAddress', ['emailAddress' => $emailAddressInput]);
        } elseif ($nameInput) {
            return $this->redirectToRoute('searchByName', ['name' => $nameInput]);
        } elseif ($deliveryInput) {
            return $this->redirectToRoute('searchByDelivery', ['deliveryName' => $deliveryInput]);
        }

        return $this->redirectToRoute('callInfoHome');
    }

    /**
     * @route("/search/phone/{phoneNumber}", name="searchByPhone")
     */
    public function retrieveInfoViaPhoneNumber(string $phoneNumber): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByPhoneNumber($phoneNumber);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }

    /**
     * @route("/search/name/{name}", name="searchByName")
     */
    public function retrieveInfoViaName(string $name): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByName($name);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }

    /**
     * @route("/search/postal/{postalCode}", name="searchByPostal")
     */
    public function retrieveInfoViaPostalCode(string $postalCode): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByZipCode($postalCode);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }

    /**
     * @route("/search/delivery/{deliveryName}", name="searchByDelivery")
     */
    public function retrieveInfoViaDeliveryName(string $deliveryName): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByDeliveryName($deliveryName);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }

    /**
     * @route("/search/order/{orderNumber}", name="searchByOrderNumber")
     */
    public function retrieveInfoViaOrderNumber(string $orderNumber): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByOrderNumber($orderNumber);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }

    /**
     * @route("/search/email/{emailAddress}", name="searchByEmailAddress")
     */
    public function retrieveInfoViaEmailAdress(string $emailAddress): Response
    {
        $callInfoProvider = new CallInfoProvider();
        $customers = $callInfoProvider->getAllInfoByEmailAddress($emailAddress);

        return $this->render('call-info-data.html.twig', ['customers' => $customers]);
    }
}
