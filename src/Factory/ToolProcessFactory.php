<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\ToolProcess;
use App\Provider\ToolProcessProvider;

class ToolProcessFactory
{
    /**
     * Create process object
     *
     * @param string $name of new process
     */
    public static function create(string $name, array $options = []): object
    {
        // todo: check if id doesnt exist
        $toolProvider = new ToolProcessProvider();
        $toolProcess = new ToolProcess($toolProvider->createId(), $name);
        $toolProcess->setOptions($options);

        return $toolProcess;
    }
}
