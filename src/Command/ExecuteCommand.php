<?php

declare(strict_types=1);

namespace App\Command;

use App\Provider\ServiceAdapter;
use App\Provider\ServiceManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'app:execute';

    protected function configure(): void
    {
        $this->setName('app:execute')
            ->setDescription('Run any command defined in the service option mapper.')
            ->addArgument('appcommand', InputOption::VALUE_REQUIRED, 'The command to run');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $serviceManager = new ServiceManager();
        $serviceManager->fromMapper($input->getArgument('appcommand'));
        $service = $serviceManager->getService();
        $serviceAdapter = new ServiceAdapter($service);
        $serviceAdapter->execute();
    }
}
