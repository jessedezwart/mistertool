<?php

declare(strict_types=1);

namespace App\Model;

use JsonSerializable;

class StockProduct implements JsonSerializable
{
    /**
     * @var string
     */
    protected $sku;
    /**
     * @var int
     */
    protected $odooProductProductId;
    /**
     * @var int
     */
    protected $odooProductTemplateId;
    /**
     * @var string
     */
    protected $locationName = 'Not found';
    /**
     * @var int
     */
    protected $locationId;
    /**
     * @var int
     */
    protected $quantity = 0;
    /**
     * @var string[]
     */
    protected $supplierInformation = [];

    public function __construct(string $sku)
    {
        $this->sku = $sku;
    }

    public function jsonSerialize(): array
    {
        return [
            'sku' => $this->sku,
            'product_id' => $this->odooProductProductId,
            'product_template_id' => $this->odooProductTemplateId,
            'location_name' => $this->locationName,
            'location_id' => $this->locationId,
            'quantity' => $this->quantity,
            'supplier_information' => $this->supplierInformation,
        ];
    }

    /**
     * Get the value of odooProductProductId
     */
    public function getOdooProductProductId(): int
    {
        return $this->odooProductProductId;
    }

    /**
     * Set the value of odooProductProductId
     */
    public function setOdooProductProductId(int $odooProductProductId): self
    {
        $this->odooProductProductId = $odooProductProductId;

        return $this;
    }

    /**
     * Get the value of locationName
     */
    public function getLocationName(): string
    {
        return $this->locationName;
    }

    /**
     * Set the value of locationName
     */
    public function setLocationName(string $locationName): self
    {
        $this->locationName = $locationName;

        return $this;
    }

    /**
     * Get the value of locationId
     */
    public function getLocationId(): int
    {
        return $this->locationId;
    }

    /**
     * Set the value of locationId
     */
    public function setLocationId(int $locationId): self
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get the value of quantity
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get the value of supplierInformation
     */
    public function getSupplierInformation(): array
    {
        return $this->supplierInformation;
    }

    /**
     * Set the value of supplierInformation
     */
    public function setSupplierInformation(array $supplierInformation): self
    {
        $this->supplierInformation = $supplierInformation;

        return $this;
    }

    /**
     * Get the value of odooProductTemplateId
     */
    public function getOdooProductTemplateId(): int
    {
        return $this->odooProductTemplateId;
    }

    /**
     * Set the value of odooProductTemplateId
     */
    public function setOdooProductTemplateId(int $odooProductTemplateId): self
    {
        $this->odooProductTemplateId = $odooProductTemplateId;

        return $this;
    }
}
