<?php

declare(strict_types=1);

namespace App\Model\Service;

class Service
{
    /**
     * @var array
     */
    public $options = [];

    public function setOptions(array $options): void
    {
        $this->options = $options;
    }

    public function execute(): void
    {
    }
}
