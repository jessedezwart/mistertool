<?php

declare(strict_types=1);

namespace App\Model\Service;

use MultiTool\Command\ConvertCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;

class MultiToolService extends Service
{
    public function execute(): void
    {
        $application = new Application();
        $application->setAutoExit(false); // ensures the program continues after executing the task
        $application->add(new ConvertCommand());

        $input = new ArrayInput($this->options['options']);

        $application->run($input);
    }
}
