<?php

declare(strict_types=1);

namespace App\Model\Service;

class ShellExecService extends Service
{
    public function execute(): void
    {
        $cmd = $this->options['options']['command'];
        $proc = popen($cmd, 'r');
        while (! feof($proc)) {
            echo fread($proc, 4096);
        }
    }
}
