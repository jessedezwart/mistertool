<?php

declare(strict_types=1);

namespace App\Model;

class ToolProcess
{
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string[]
     */
    protected $options = [];

    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Get the value of id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of options
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Set the value of options
     */
    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }
}
