<?php

declare(strict_types=1);

namespace App;

class ServiceOptionMapper
{
    /**
     * All accessible services
     *
     * @var array
     */
    private static $map = [
        'odoo:order:sparex' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'ssh -i /var/www/symfony/ssh.key -o StrictHostKeyChecking=no root@odoo.w4e.com /usr/bin/php /var/www/stock-price-updater-1.0.1/src/order.php --supplier sparex --vendor Sparex --sleep 0 --buy --confirm-odoo',
            ],
        ],
        'odoo:order:fournial' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'ssh -i /var/www/symfony/ssh.key -o StrictHostKeyChecking=no root@odoo.w4e.com/usr/bin/php /var/www/stock-price-updater-1.0.1/src/order.php  --supplier fournial --vendor Groupe-Sterenn --buy --confirm-odoo',
            ],
        ],
        'odoo:target:read' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'php /opt/legacy/OdooOmzetScore.php',
            ],
        ],
        'odoo:order:granit' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'ssh -i /var/www/symfony/ssh.key -o StrictHostKeyChecking=no root@odoo.w4e.com/usr/bin/bash /usr/local/bin/granit-order.bash',
            ],
        ],
        'odoo:order' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'ssh -i /var/www/symfony/ssh.key -o StrictHostKeyChecking=no root@odoo.w4e.com /usr/bin/php /var/www/stock-price-updater-1.0.1/src/order.php --supplier <<supplier>> --vendor <<vendor>> --sleep 0',
            ],
        ],
        'granit:product:add' => [
            'type' => 'ShellExecService',
            'options' => [
                'command' => 'php /opt/legacy/crawlGranit.php "<<granit-sku>>"',
            ],
        ],
        'odoo:jobs:retrigger' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/requeue.yml',
            ],
        ],
        'magento:shipping:update' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/ship-orders.yml',
            ],
        ],
        'odoo:availability:read' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/odoo-availability-check.yml',
            ],
        ],
        'odoo:margin:read' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/odoo-margin-read.yml',
            ],
        ],
        'odoo:view:read' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/odoo-view-read.yml',
            ],
        ],
        'odoo:order:check' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/odoo-order-check.yml',
            ],
        ],
        // 'fix:partners' => [
        //     'type' => 'MultiToolService',
        //     'options' => [
        //         'command' => 'tool:convert',
        //         '-c' => __DIR__ . '/../config/tools/fix-shite.yml',
        //     ],
        // ],
        'update:orderlines' => [
            'type' => 'MultiToolService',
            'options' => [
                'command' => 'tool:convert',
                '-c' => __DIR__ . '/../config/tools/update-orderline.yml',
            ],
        ]
    ];

    public static function getMap(): array
    {
        return self::$map;
    }
}
