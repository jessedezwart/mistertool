<?php

declare(strict_types=1);

namespace App\Element\Convert;

use HttpClients\ClientFactory;
use MultiTool\Element\Convert\AbstractConverter;
use W4eMagento\Api\MagentoService;
use W4eMagento\Provider\OrderProvider;

/**
 * @author Nick Dekker
 */
class OdooOrderCheckConverter extends AbstractConverter
{
    private $conversion = [];
    protected $orderProvider;
    /**
     * @var \HttpClients\OdooClientSync
     */
    protected $odooOrderClient;

    public function __construct() {
        $settings = [
            'store_url' => 'https://www.misteragri.com/index.php/rest',
            'version' => 'V1',
            'user' => 'nickdekker',
            'password' => 'wsBaLBYBagFRiRWcUHvd8vocwGekVfDuyZTrU3dI',
            'role' => 'admin',
        ];
        $api = new MagentoService($settings);
        $this->orderProvider = new OrderProvider($api);
        $clientFactory = new ClientFactory();
        $this->odooOrderClient = $clientFactory->getOdooSyncClient('sale.order', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    public function execute($data)
    {
        return $this->convert($data);
    }

    public function convert($data)
    {
        $magentoOrder = $this->orderProvider->getOrder($data["magento_id"]);
        if (!property_exists($magentoOrder, "state")) return $data;
        if ($magentoOrder->state === "processing" || $magentoOrder->state === "complete") {
            // Update odoo state
            $this->odooOrderClient->callButton("action_confirm", [$data["id"]]);
        }
        echo $data["magento_order_reference"] . PHP_EOL;
    }

    public function getConversions()
    {
        return $this->conversion;
    }

}
