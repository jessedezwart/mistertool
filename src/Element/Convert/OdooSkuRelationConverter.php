<?php

declare(strict_types=1);

namespace App\Element\Convert;

use MultiTool\Element\Convert\AbstractConverter;

class OdooSkuRelationConverter extends AbstractConverter
{
    private $conversion = [];

    public function execute($data)
    {
        return $this->convert($data);
    }

    /**
     * Receives product and alter
     */
    public function convert($data)
    {
        $newData['supplier_code'] = $data['name'][0];
        $newData['supplier_canonical'] = $data['name'][1];
        $slug = mb_strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $data['name'][1])));
        $newData['supplier_slug'] = $slug;
        $newData['supplier_sku'] = $data['product_code'];

        // Retrieve sku of the product
        if (isset($data['product_tmpl_id'][0])) {
            try {
                $sellerProvider = new \MultiTool\Service\OdooService('product.template', [['id', '=', $data['product_tmpl_id'][0]]], 'default_code');
                if ($sellerProvider->valid()) {
                    $newData['sku'] = $sellerProvider->current()->getSku();
                }
            } catch (\Throwable $th) {
            }
        }

        return $newData;
    }

    public function getConversions()
    {
        return $this->conversion;
    }
}
