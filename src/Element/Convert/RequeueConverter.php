<?php

declare(strict_types=1);

namespace App\Element\Convert;

use MultiTool\Element\Convert\AbstractConverter;

/**
 * Description of AbstractConverter
 *
 * @author Nick Dekker
 */
class RequeueConverter extends AbstractConverter
{
    private $conversion = [];
    protected $api = null;
    protected $status = false;

    public function __construct()
    {
    }

    /**
     * Receives product and alter
     */
    public function convert($data)
    {
        var_dump($data);

        return [
            'id' => $data['id'],
            'state' => 'pending',
        ];
    }

    public function getConversions()
    {
        return $this->conversion;
    }
}
