<?php

declare(strict_types=1);

namespace App\Element\Convert;

use HttpClients\ClientFactory;
use MultiTool\Element\Convert\AbstractConverter;
use W4eMagento\Api\MagentoService;
use W4eMagento\Provider\OrderProvider;

/**
 * @author Nick Dekker
 */
class FixPartnersConverter extends AbstractConverter
{
    private $conversion = [];
    /**
     * @var \HttpClients\OdooClientSync
     */
    protected $odooDeliveryClient;

    public function __construct() {
        $clientFactory = new ClientFactory();
        $this->odooDeliveryClient = $clientFactory->getOdooSyncClient('stock.picking', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    public function execute($data)
    {
        return $this->convert($data);
    }

    public function convert($data)
    {
        $partnerId = $data["partner_id"][0];
        foreach ($data["picking_ids"] as $pickingId) {
            $this->odooDeliveryClient->updateItem($pickingId, ["partner_id" => $partnerId]);
        }

        echo $data["magento_order_reference"] . PHP_EOL;

        return $data;
    }

    public function getConversions()
    {
        return $this->conversion;
    }

}
