<?php

declare(strict_types=1);

namespace App\Element\Convert;

use DateInterval;
use DateTime;
use HttpClients\ClientFactory;
use MultiTool\Element\Convert\AbstractConverter;
use W4eMagento\Api\MagentoService;
use W4eMagento\Provider\OrderProvider;

class ShipMagentoOrdersConverter extends AbstractConverter
{
    protected $api = null;
    protected $orderProvider;
    protected $odooMagentoSaleOrderClient;
    protected $odooDeliveryClient;
    protected $odooMoveLineClient;

    public function __construct()
    {
        $settings = [
            'store_url' => 'https://www.misteragri.com/index.php/rest',
            'version' => 'V1',
            'user' => 'nickdekker',
            'password' => 'wsBaLBYBagFRiRWcUHvd8vocwGekVfDuyZTrU3dI',
            'role' => 'admin',
        ];
        $this->api = new MagentoService($settings);
        $clientFactory = new ClientFactory();
        $this->odooMagentoSaleOrderClient = $clientFactory->getOdooSyncClient('magento.sale.order', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooDeliveryClient = $clientFactory->getOdooSyncClient('stock.picking', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooMoveLineClient = $clientFactory->getOdooSyncClient('stock.move', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->orderProvider = new OrderProvider($this->api);
    }

    /**
     * Receives product and alter
     */
    public function execute($data)
    {
        echo $data['name'];
        // Check time order
        $orderTime = DateTime::createFromFormat('Y-m-d H:i:s', $data['date_order']);
        $fromDate = new DateTime();
        $untilDate = new DateTime();
        $fromDate->sub(new DateInterval('P2M'));
        $untilDate->sub(new DateInterval('P0D'));
        $doOrder = $fromDate < $orderTime && $untilDate > $orderTime;

        if (! $doOrder) {
            echo 'Skipped, cause time.';

            // Skip if not in time range
            return $data;
        }
        // Get deliveries
        foreach ($data['picking_ids'] as $deliveryId) {
            $delivery = $this->getDeliveryById($deliveryId);
            if (('processing' !== $delivery->magento_order_status || 'done' !== $delivery->state) && 'outgoing' === $delivery->picking_type_code) {
                echo 'Skipped, cause not ready.';

                return $data;
            }
        }

        $magentoOrder = $this->odooMagentoSaleOrderClient->getItem($data['magento_bind_ids'][0]);
        $magentoOrderId = $magentoOrder->result->records[0]->magento_id;
        try {
            $this->shipOrder($magentoOrderId);
        } catch (\Throwable $th) {
            var_dump($th);
        }

        return $data;
    }

    private function getDeliveryById($id)
    {
        $result = $this->odooDeliveryClient->getItem($id);

        if ($result) {
            return $result->result->records[0];
        }

        return false;
    }

    private function shipOrder($magentoOrderId): void
    {
        $this->orderProvider->shipOrder($magentoOrderId);
    }
}
