<?php

declare(strict_types=1);

namespace App\Element\Convert;

use MultiTool\Element\Convert\AbstractConverter;

class OdooMarginFormatterConverter extends AbstractConverter
{
    public function execute($data)
    {
        return $this->convert($data);
    }

    /**
     * Receives product and alter
     */
    public function convert($data)
    {
        $matches = [];
        $regex = '/Stuks per (\d*)/m';
        preg_match_all($regex, $data['uom_po_id'][1], $matches);

        if (empty($matches[0])) {
            $processed['vpe'] = 1;
        } else {
            $processed['vpe'] = $matches[1][0];
        }
        $processed['name'] = $data['name'];
        $processed['sku'] = $data['default_code'];
        $processed['cost_price'] = $data['price'];
        $processed['odoo_id'] = $data['id'];

        echo $processed['sku'] . PHP_EOL;

        return $processed;
    }
}
