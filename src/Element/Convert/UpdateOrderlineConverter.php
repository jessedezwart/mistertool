<?php

declare(strict_types=1);

namespace App\Element\Convert;

use HttpClients\ClientFactory;
use MultiTool\Element\Convert\AbstractConverter;
use W4eMagento\Api\MagentoService;
use W4eMagento\Provider\OrderProvider;

/**
 * @author Nick Dekker
 */
class UpdateOrderlineConverter extends AbstractConverter
{
    private $conversion = [];
    /**
     * @var \HttpClients\OdooClientSync
     */
    protected $odooOrderLineClient;
    /**
     * @var \HttpClients\OdooClientSync
     */
    protected $odooProductClient;
    /**
     * @var \HttpClients\OdooClientSync
     */
    protected $odooOrderClient;

    public function __construct() {
        $clientFactory = new ClientFactory();
        $this->odooOrderLineClient = $clientFactory->getOdooSyncClient('purchase.order.line', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
        $this->odooProductClient = $clientFactory->getOdooSyncClient('product.product', $_ENV['ODOO_HOST'], [], $_ENV['ODOO_DATABASE'], $_ENV['ODOO_USERNAME'], $_ENV['ODOO_PASSWORD']);
    }

    public function execute($data)
    {
        return $this->convert($data);
    }

    public function convert($data)
    {
        $vpeRegex = '/Stuks per (\d*)/m';
        echo "Checking orderlines of " . $data["partner_id"][1] . PHP_EOL;
        foreach ($data["order_line"] as $orderlineId) {
            $criteria = [['id', '=', $orderlineId]];

            // Get product id from orderline
            try {
                $orderline = $this->odooOrderLineClient->searchRead($criteria, 0, 1, ["product_id", "product_qty"])->result->records[0];   
            } catch (\Throwable $th) {
                echo "Couldn't get orderline model of $orderlineId" . PHP_EOL;
                continue;
            }
            $productId = $orderline->product_id[0];
            $currentQty = $orderline->product_qty;

            // Get additional data from product model
            $criteria = [['id', '=', $productId]];
            try {
                $product = $this->odooProductClient->searchRead($criteria, 0, 1, ["virtual_available","reordering_min_qty", "uom_po_id", "default_code"])->result->records[0];
            } catch (\Throwable $th) {
                echo "Couldn't get product model of $productId" . PHP_EOL;
                continue;
            }
            $forecasted = $product->virtual_available;
            $minQty = $product->reordering_min_qty;
            
            // Get uom id or regex
            if ($product->uom_po_id[0] == 1) {
                $uom = 1;
            } else {
                preg_match_all($vpeRegex, $product->uom_po_id[1], $matches, PREG_SET_ORDER, 0);
                $uom = $matches[0][1];
            }

            // Calculate product quantity
            $needed = $minQty - $forecasted;
            if ($uom > 1 && $needed > 0) {
                $needed = $this->ceilToAny($needed, $uom) / $uom;
            }
            if ($needed < 0) {
                $needed = 0;
            }

            // Just some output for easy chechking
            echo "    " . $product->default_code . ": orderline_qty = $currentQty, vpe = $uom, forecasted = $forecasted, min_qty = $minQty, needed = $needed" . PHP_EOL;

            // Delete or update
            if ($needed == 0) {
                $this->odooOrderLineClient->deleteItem($orderlineId);
            } else {
                $this->odooOrderLineClient->updateItem($orderlineId, ["product_qty" => $needed]);
            }
        }
        return $data["id"];
    }

    public function getConversions()
    {
        return $this->conversion;
    }

    function ceilToAny($numberToRound, $roundTo) {
        return round(($numberToRound+$roundTo/2)/$roundTo)*$roundTo;
    }

}
