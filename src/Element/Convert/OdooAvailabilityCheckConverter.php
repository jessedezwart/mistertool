<?php

declare(strict_types=1);

namespace App\Element\Convert;

use MultiTool\Communicator\OdooConnector;
use MultiTool\Element\Convert\AbstractConverter;

/**
 * @author Nick Dekker
 */
class OdooAvailabilityCheckConverter extends AbstractConverter
{
    private $conversion = [];

    public function execute($data)
    {
        return $this->convert($data);
    }

    /**
     * Receives product and alter
     */
    public function convert($data)
    {
        $id = $data['id'];
        $moveLines = $data['move_lines'];
        if (! $data['move_lines']) {
            return [];
        }

        $problems = [];
        $probCnt = 0;
        $waitCnt = 0;
        foreach ($moveLines as $move) {
            $sellerProvider = new \MultiTool\Service\OdooService(
                'stock.move',
                [['id', '=', $move]],
                'product_uom_qty,qty_available,product_id,picking_id,reserved_availability'
            );
            $moveLine = $sellerProvider->current()->toArray(true);
            $qty = $moveLine['product_uom_qty'];
            $problemItems = '';
            if (0 === $moveLine['reserved_availability']) {
                $waitCnt++;
                // var_dump($moveLine['product_id']);
                $sellerProvider = new \MultiTool\Service\OdooService('product.product', [['id', '=', $moveLine['product_id'][0]]], 'qty_available');
                $item = $sellerProvider->current()->toArray(true);
                if ($item['qty_available'] >= $qty) {
                    $probCnt++;
                    echo 'warning, in line' . $moveLine['picking_id'][0] . '<br>' . PHP_EOL;
                    $problemItems = $item['id'] . ',';
                }
            }
            if ('' !== $problemItems) {
                $problems = [
                    'id' => $id,
                    'items' => $problemItems,
                ];
            }
        }

        if (! empty($problems)) {
            // 4 , 3
            if ($waitCnt > $probCnt) {
                return [];
            }
            var_dump('Order geforceerd!');
            self::sendSlackMessage("[order forced] http://odoo.w4e.com/web#id=${id}&view_type=form&model=stock.picking&menu_id=628&action=902");
            $queueOdoo = new OdooConnector('stock.picking', [['id', '=', $id]], []);
            $queueOdoo->callButton('force_assign', [[$id]]);
        }

        return $problems;
    }

    public function getConversions()
    {
        return $this->conversion;
    }

    public static function sendSlackMessage($message): void
    {
        $body = json_encode([
            'text' => $message,
        ]);

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://hooks.slack.com/services/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',
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
            ],
        ]);
        curl_exec($curl);
        curl_close($curl);
    }

    public static function sendPb($title, $msg): void
    {
        $post = [];
        $post['body'] = $msg;
        $post['title'] = $title;
        $post['type'] = 'note';

        $json = json_encode($post);
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://api.pushbullet.com/v2/pushes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => [
                'access-token: o.0GvwOIs4YsDwqkwT7k7LnppfjYh2iBqU',
                'content-type: application/json',
            ],
        ]);
        curl_exec($curl);

        $err = curl_error($curl);
        if ($err) {
            echo $err;
        }
        curl_close($curl);
    }
}
