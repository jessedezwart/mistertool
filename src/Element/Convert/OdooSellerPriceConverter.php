<?php

declare(strict_types=1);

namespace App\Element\Convert;

use MultiTool\Element\Convert\AbstractConverter;

class OdooSellerPriceConverter extends AbstractConverter
{
    private $conversion = [];

    public function execute($data)
    {
        return $this->convert($data);
    }

    /**
     * Receives product and alter
     */
    public function convert($data)
    {
        $id = $data['id'];
        if (! $data['seller_ids']) {
            $data['price'] = 0;

            return $data;
        }

        $sellerProvider = new \MultiTool\Service\OdooService('product.supplierinfo', [['product_tmpl_id', '=', $id]], '');

        $price = 0;
        $sellerCount = 0;
        foreach ($sellerProvider as $seller) {
            $sellerCount++;
            $price = $seller->getPrice();
        }

        $priorityTable = [];
        if ($sellerCount > 1) {
            foreach ($sellerProvider as $seller) {
                if (0 === $seller->getPrice()) {
                    continue;
                }

                $tempPriorityTable = [];
                $tempPriorityTable['seller'] = $seller;
                $tempPriorityTable['priority'] = 0;

                switch ($seller->getName()[1]) {
                    case 'Granit-Parts':
                        $tempPriorityTable['priority'] = 1;
                        break;
                    case 'Sparex':
                        $tempPriorityTable['priority'] = 2;
                        break;
                    case 'Groupe-Sterenn':
                        $tempPriorityTable['priority'] = 3;
                        break;
                    case 'Laryna - Miralbueno':
                        $tempPriorityTable['priority'] = 4;
                        break;
                    case 'Yerik International':
                        $tempPriorityTable['priority'] = 5;
                        break;
                    default:
                        $tempPriorityTable['priority'] = 100;
                        break;
                }

                $priorityTable[] = $tempPriorityTable;
            }
            if (! empty($priorityTable)) {
                $priorityColumn = array_column($priorityTable, 'priority');
                $key = array_keys($priorityColumn, min($priorityColumn), true);
                $highestPriority = $priorityTable[$key[0]];
            }

            if ($highestPriority) {
                $highestPriority['seller']->getPrice();
            }
        }

        $data['price'] = $price;

        return $data;
    }

    public function getConversions()
    {
        return $this->conversion;
    }
}
